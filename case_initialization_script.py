
import modules.clinbiojira as clinbiojira
from pycipapi.cipapi_client import CipApiClient
import os


# Prompts the user for a referral ID as input
group_id = str(input("Please enter your referral ID: "))

# Connects to the CIP-API client using the provided credentials. If the connection fails, it raises a system exit error
try:
    cipapi = CipApiClient(
        url_base='https://cipapi.test.aws.gel.ac',
        user=os.getenv('GEL_EMAIL'),
        password=os.getenv('GEL_PASSWORD'),
        retries=1, 
        fixed_paramters={"reports_v6": "true"}
        )
except:
    raise SystemExit(
    'ERROR. Could not connect with CIPAPI'
    )

# It fetches case data for the given referral ID and extracts workspace and sample IDs from the pedigree members
cases = cipapi.get_cases(group_id=group_id)

for case in cases:
    case_id = case.case_id
    sample_type = case.sample_type

    case = cipapi.get_case(case_id=case.interpretation_request_id, case_version=case.version)
    ir = case.interpretation_request_data['json_request']

    workspace = ir['workspace']

    sample_ids = []
    for member in ir['pedigree']['members']:
        samples = member.get('samples')
        if samples:
            sample_id = samples[0]['sampleId']
            sample_ids.append(sample_id)

# Connects to the GEL JIRA client using the provided credentials
jira_client = clinbiojira.JiraClient(f"{os.getenv('GEL_USERNAME')}", f"{os.getenv('GEL_PASSWORD')}")

# Searches for issues related to the referral ID. If issues are found, it prints the issue key(s) and a warning if more than one issue is found
jira_issues = jira_client.search_issue('IPCASES', group_id)

# If no issues are found, it creates a new issue with the referral ID and prints the issue key
if jira_issues:
    issue_key = jira_issues[0].key
    for issue in jira_issues:
        print(f"- An issue already exists for your referral, {issue_key}")
    if len(jira_issues) > 1:
        print(f"- More than one issue has been found for this referral. Only {issue_key} will be acknowledged, please remove the rest")
else:
    issue_key = jira_client.create_issue('IPCASES', group_id)
    print(f"- No issues found for this referral, a new one has been created with key {issue_key}")


# The script updates the description of the issue with the group ID, sample type, and sample IDs and workspace
new_description = f"Group ID: {group_id}\nSample type: {sample_type}\nSample IDs: {sample_ids}\nWorkspace: {workspace}"

jira_client.update_issue(issue_key, new_description)

print(f'- The description of the issue has been updated')
