from jira import JIRA

class JiraClient:

    def __init__(self, username, password):
        self.options = {"server": "https://jira.extge.co.uk/"}
        self.username = username
        self.password = password
        self.jira = None

    def login(self):
        self.jira = JIRA(self.options, basic_auth=(self.username, self.password))
        try:
            jira = JIRA(self.options, basic_auth=(self.username, self.password))
            return jira
        except Exception as e:
            print("Login failed, make sure your credentials are correct")
            return None

    def search_issue(self, project, group_id):
        if not self.jira:
            self.login()
        jql_query = f'project="{project}" and summary ~ "{group_id}"'
        issues = self.jira.search_issues(jql_query)
        return issues

    def create_issue(self, project, group_id):
        if not self.jira:
            self.login()
        issue_dict = {
            'project': {'key': project},
            'summary': group_id,
            'issuetype': {'name': 'Story'}
        }
        new_issue = self.jira.create_issue(fields=issue_dict)
        return new_issue.key

    def update_issue(self, issue_key, new_description):
        if not self.jira:
            self.login()
        issue = self.jira.issue(issue_key)
        issue.update(description=new_description)